#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Vector_BPC.h"

//dec guard
#ifndef CONSOLE_CONTROL
#define CONSOLE_CONTROL

void ShowHelp(std::string& userCommand);
void ShowInvalid(std::string& invalidCommand);
void DefinesteUnVectorLoop(std::string& userComand, VectorBPC *&vector);
int ParseInt(std::string& input);
float ParseFloat(std::string& input);
double ParseDouble(std::string& input);
void AssignValue(int& xReal, float& xRational, double& xRational2, std::string& input);
void DefinesteMatriceLoop(std::string& userCommand);
void ListeazaVectori(std::string& userCommand, std::vector<VectorBPC*>& vectori);
void StergeLista(std::string& userCommand, std::vector<VectorBPC*>& vectori);
#endif // !CONSOLE_CONTROL

