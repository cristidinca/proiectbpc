#pragma once
#ifndef VECTORIBPC
#define VECTORIBPC
class VectorBPC
{
private:
	int			_vectorReal[3];
	float		_vectorRational[3];
	double		_vectorRational2[3];
public:
	bool initRealMembers		= false;
	bool initRationalMembers	= false;
	bool initRational2Members	= false;
	void GetReal(int& a, int& b, int& c);
	void GetRational(int& a, int& b, int& c);
	void GetRational2(int& a, int& b, int& c);
	int SizeOfReal();
	int SizeOfRational();
	int SizeOfRational2();
	VectorBPC(int		members[]);
	VectorBPC(float		members[]);
	VectorBPC(double	members[]);
};
#endif // !VECTOR

