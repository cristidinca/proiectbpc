#include "pch.h"
#include <iostream>
#include <string>
#include <vector>

#include "CLICtrl.h"
#include "Constants.h"
#include "Vector_BPC.h"
using namespace std;

void ShowHelp(string& userCommand)
{
	cout << HELP_TEXT_0 << HELP_TEXT_1 << HELP_TEXT_2 << HELP_TEXT_3 << HELP_TEXT_4 << HELP_TEXT_5 << HELP_TEXT_6;
	userCommand = WAIT_INPUT_CMD;
}

void ShowInvalid(string& invalidCommand)
{
	cout << INVALID_TEXT_1 << invalidCommand << INVALID_TEXT_2;
	invalidCommand = WAIT_INPUT_CMD;
}

void DefinesteUnVectorLoop(string& userCommand, VectorBPC *&vector)
{
	VectorBPC *vectorNou = nullptr;
	int aReal = NULL, bReal = NULL, cReal = NULL;
	float aRational = NULL, bRational = NULL, cRational = NULL;
	double aRational2 = NULL, bRational2 = NULL, cRational2 = NULL;
	int membriReali[3];
	float membriRationali[3];
	double membriRationali2[3];
	int constructorulAles = -1;
	int iterations = 0;
	while (((aReal == NULL || bReal == NULL || cReal == NULL)
		|| (aRational == NULL || bRational == NULL || cRational == NULL)
		|| (aRational2 == NULL || bRational2 == NULL || cRational2 == NULL)) && iterations < 3)
	{
		string input = "";
		try
		{
			if (aReal == NULL && aRational == NULL && aRational2 == NULL)
			{
				cout << "-->a: ";
				getline(cin, input);

				AssignValue(aReal, aRational, aRational2, input);
			}
			else if (bReal == NULL && bRational == NULL && bRational2 == NULL)
			{
				cout << "-->b: ";
				getline(cin, input);

				AssignValue(bReal, bRational, bRational2, input);
			}
			else if (cReal == NULL && cRational == NULL && cRational2 == NULL)
			{
				cout << "-->c: ";
				getline(cin, input);

				AssignValue(cReal, cRational, cRational2, input);
			}
		}
		catch (const std::exception& e)
		{
			cout << e.what();
			//userCommand = WAIT_INPUT_CMD;
		}
		iterations++;
	}
	if (aReal != NULL && bReal != NULL && cReal != NULL)
	{
		cout << "a =" << aReal << "b = " << bReal << "c = " << cReal << "\n\n";
		membriReali[0] = aReal;
		membriReali[1] = bReal;
		membriReali[2] = cReal;
		constructorulAles = 0;
	}
	else if (aRational != NULL && bRational != NULL && cRational != NULL)
	{
		cout << "a =" << aRational << "b = " << bRational << "c = " << cRational << "\n\n";
		membriRationali[0] = aRational;
		membriRationali[1] = bRational;
		membriRationali[2] = cRational;
		constructorulAles = 1;
	}
	else if (aRational2 != NULL && bRational2 != NULL && cRational2 != NULL)
	{
		cout << "a =" << aRational2 << "b = " << bRational2 << "c = " << cRational2 << "\n\n";
		membriRationali2[0] = aRational2;
		membriRationali2[1] = bRational2;
		membriRationali2[2] = cRational2;
		constructorulAles = 2;
	}
	else
	{
		cout << "Numerele trebuie sa faca parte din aceeasi familie de numere (Reale sau Rationale).\n";
		aRational = NULL, bRational = NULL, cRational = NULL;
		aRational2 = NULL, bRational2 = NULL, cRational2 = NULL;
		DefinesteUnVectorLoop(userCommand, vector);
	}

	switch (constructorulAles)
	{
	case 0:
		vectorNou = new VectorBPC(membriReali);
		vectorNou->initRealMembers = true;
		break;
	case 1:
		vectorNou = new VectorBPC(membriRationali);
		vectorNou->initRationalMembers = true;
		break;
	case 2:
		vectorNou = new VectorBPC(membriRationali2);
		vectorNou->initRational2Members = true;
		break;
	default:
		break;
	}
	vector = vectorNou;
	userCommand = WAIT_INPUT_CMD;
}

int ParseInt(string& input)
{
	for (int i = 0; i < (int)input.length(); i++)
		if (input[i] == '.')
			return NULL;

	int toReturn = NULL;
	try 
	{
		toReturn = stoi(input);
	}
	catch(exception e)
	{
		cout << e.what() << "\n" << "Incearca un numar intreg sau rational si foloseste caracterul \".\" drept separator" << "\n";
	}
	return toReturn;
}

float ParseFloat(string& input)
{
	if (input.length() == 4)
		return NULL;

	float toReturn = NULL;
	try
	{
		toReturn = stof(input);
	}
	catch (exception e)
	{
		cout << e.what() << "\n" << "Incearca un numar intreg sau rational si foloseste caracterul \".\" drept separator" << "\n";
	}
	return toReturn;
}

double ParseDouble(string& input)
{
	double toReturn = NULL;
	try
	{
		toReturn = stod(input);
	}
	catch (exception e)
	{
		cout << e.what() << "\n" << "Incearca un numar intreg sau rational si foloseste caracterul \".\" drept separator" << "\n";
	}
	return toReturn;
}

void AssignValue(int& xReal, float& xRational, double& xRational2, string& input) 
{
	int tries = 0;
	while ((xReal == NULL || xRational == NULL || xRational2 == NULL) && tries < 3)
	{
		xReal = ParseInt(input);
		if (xReal != NULL)
			break;
		tries++;
		xRational = ParseFloat(input);
		if (xRational != NULL)
			break;
		tries++;
		xRational2 = ParseDouble(input);
		tries++;
	}
}

void DefinesteMatriceLoop(string& userCommand)
{
	const int linii = 4;
	const int coloane = 4;
	char matrice[linii][coloane];
	string input = "";
	for (int l = 0; l < linii; l++)
	{
		for (int c = 0; c < coloane; c++)
		{
			cout << "Tasteaza caracterul pentru casuta " << l << " X " << c << ":";
			getline(cin, input);
			matrice[l][c] = input[0];
		}
	}

	for (int l = 0; l < linii; l++)
	{
		cout << "____________________\n";
		for (int c = 0; c < coloane; c++)
		{
			cout << "| " << matrice[l][c] << " |";
		}
		cout << "\n";
	}
	cout << "____________________\n";

	userCommand = WAIT_INPUT_CMD;
}

void ListeazaVectori(string& userCommand, vector<VectorBPC*>& vectori)
{
	int a = 0, b = 0, c = 0;
	for (int i = 0; i < vectori.size(); i++)
	{
		if (vectori[i]->initRealMembers)
		{
			vectori[i]->GetReal(a, b, c);
			cout << "[" << a << "," << b << "," << c << "," << "]" << "\n";
		}
		else if (vectori[i]->initRationalMembers)
		{
			vectori[i]->GetRational(a, b, c);
			cout << "[" << a << "," << b << "," << c << "," << "]" << "\n";
		}
		else if (vectori[i]->initRational2Members)
		{
			vectori[i]->GetRational2(a, b, c);
			cout << "[" << a << "," << b << "," << c << "," << "]" << "\n";
		}
	}
	userCommand = WAIT_INPUT_CMD;
}

void StergeLista(string& userCommand, vector<VectorBPC*>& vectori)
{
	vectori.clear();
	userCommand = WAIT_INPUT_CMD;
}