#include "pch.h"
#include "Vector_BPC.h"
//#include <iostream>
using namespace std;
VectorBPC::VectorBPC(int members[]) 
{
	for (int i = 0; i < sizeof(members); i++)
	{
		_vectorReal[i] = members[i];
	}
}
VectorBPC::VectorBPC(float members[])
{
	for (int i = 0; i < sizeof(members); i++)
	{
		_vectorRational[i] = members[i];
	}
}
VectorBPC::VectorBPC(double members[]) 
{
	for (int i = 0; i < sizeof(members); i++)
	{
		_vectorRational2[i] = members[i];
	}
}
void VectorBPC::GetReal(int& a, int& b, int& c)
{
	a = _vectorReal[0];
	b = _vectorReal[1];
	c = _vectorReal[2];
}
void VectorBPC::GetRational(int& a, int& b, int& c)
{
	a = _vectorRational[0];
	b = _vectorRational[1];
	c = _vectorRational[2];
}
void VectorBPC::GetRational2(int& a, int& b, int& c)
{
	a = _vectorRational2[0];
	b = _vectorRational2[2];
	c = _vectorRational2[3];
}
int VectorBPC::SizeOfReal()
{
	return sizeof(_vectorReal);
}
int VectorBPC::SizeOfRational() 
{
	return sizeof(_vectorRational);
}
int VectorBPC::SizeOfRational2() 
{
	return sizeof(_vectorRational2);
}

