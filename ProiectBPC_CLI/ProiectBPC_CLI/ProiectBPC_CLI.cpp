// ProiectBPC_CLI.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include"Vector_BPC.h"
#include <vector>
#include "CLICtrl.h"
#include "Constants.h"
; using namespace std;

int main()
{
	string usrCmd = HELP_CMD;
	//VectorBPC *vectori[] = { NULL };

	std::vector<VectorBPC*> vectori;

	VectorBPC vector = VectorBPC(new int[3] {0,0,0});
	VectorBPC *vectorP = &vector;
	//MainLoop(usrCmd, vector);

	while (usrCmd != EXIT_CMD)
	{
		if (usrCmd == HELP_CMD)
			ShowHelp(usrCmd);
		else if (usrCmd == DEFINE_VECTOR_CMD)
			DefinesteUnVectorLoop(usrCmd, vectorP);
		else if (usrCmd == DEFINE_MATRICE_CMD)
			DefinesteMatriceLoop(usrCmd);
		else if (usrCmd == LISTEAZA_VECTORI_CMD)
			ListeazaVectori(usrCmd, vectori);
		else if (usrCmd == STERGE_VECTORI_CMD)
			StergeLista(usrCmd, vectori);
		//else if (usrCmd == WAIT_INPUT_CMD)
		//	getline(cin, usrCmd);
		else
			ShowInvalid(usrCmd);

		getline(cin, usrCmd);

		if (vectorP != nullptr && (vectorP->initRealMembers || vectorP->initRationalMembers || vectorP->initRational2Members))
		{
			int arrLength = sizeof(vectori);
			vectori.push_back(vectorP);
			vectorP = nullptr;
		}
	}
}
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
