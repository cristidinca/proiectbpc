#pragma once

#ifndef CONSTANTS
#define CONSTANTS

//#include <iostream>
#include <string>
using namespace std;

const string HELP_TEXT_0 = "=-=-=-=-=-=-=-=-=-\nComenzi suportate:\n";
const string HELP_TEXT_1 = "\t* help\t-\tAfiseaza textul curent :)\n";
const string HELP_TEXT_2 = "\t* exit\t-\tInchide programul.\n";
const string HELP_TEXT_3 = "\t* vec\t-\tDefineste un vector Real sau Rational tridimensional\n\t\t\t----=----\n\t\t\tAlege 3 numere din multimile numerelor Reale sau Rationale.\n\t\t\tTasteaza cate un numar apoi apasa Enter.\n\t\t\tIn cazul numerelor rationale foloseste \".\" ca si separator.\n";
const string HELP_TEXT_4 = "\t* mat\t-\tDefineste o matrice de caractere (char) cu 4 linii si 4 coloane.\n\t\t\t----=----\n\t\t\tAlege 2 numere Naturale.\n\t\t\tTasteaza cate un caracter apoi apasa Enter.\n=-=-=-=-=-=-=-=-=-\n";
const string HELP_TEXT_5 = "\t* ls\t-\tAfiseaza vectorii din memorie\n";
const string HELP_TEXT_6 = "\t* clr\t-\tSterge vectorii din memorie\n";

const string INVALID_TEXT_1 = "Comanda ";
const string INVALID_TEXT_2 = " nu este recunoscuta. Tastati \"help\" pentru a vizualiza lista de comenzi cunoscute.\n";

const string HELP_CMD = "help";
const string EXIT_CMD = "exit";
const string WAIT_INPUT_CMD = "ScrieBreCeva!";
const string DEFINE_VECTOR_CMD = "vec";
const string DEFINE_MATRICE_CMD = "mat";
const string LISTEAZA_VECTORI_CMD = "ls";
const string STERGE_VECTORI_CMD = "clr";
#endif // !CONSTANTS